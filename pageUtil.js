require('/constants');

function request(argument, id) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', argument, true);
    xhr.setRequestHeader("id", id);

    xhr.onreadystatechange = function () {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            console.log('Ошибка ' + xhr.status + ':' + xhr.statusText);
            return;
        }

        console.log('Request result: ' + xhr.responseText);
        contentCreate(argument, JSON.parse(xhr.responseText));
    };

    xhr.send();
}

function contentCreate(content, data) {
    if (content == ALL_WORKERS) {
        tableCreate(data);
    } else if (content == ID_WORKER) {
        pageDetailCreate(data);
    } else if (content == ID_WORKER_CHANGE) {
        pageChangeCreate(data);
    } else if (content == ALL_RM) {
        setRm(data);
    } else if (content == WORKER_DELETE) {
        updateContentAfterDelete();
    } else if (content == SEARCH_WORKER) {
        updateContentAfterSearch(data);
    }
}

function parseDate(dateString) {
    var date = new Date(dateString);
    var options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };
    return date.toLocaleString("en-US", options);
}
function pageDetailCreate(data) {
    var body = document.getElementsByTagName('body')[0];

    var firstNameContainer = document.createElement('h3');
    var firstNameNode = document.createTextNode(data[0]["first_name"]);
    firstNameContainer.appendChild(firstNameNode);
    body.appendChild(firstNameContainer);

    var lastNameContainer = document.createElement('h3');
    var lastNameNode = document.createTextNode(data[0]["last_name"]);
    lastNameContainer.appendChild(lastNameNode);
    body.appendChild(lastNameContainer);

    var birthday = document.createElement('p');
    var birthdayNode = document.createTextNode(parseDate(data[0]["birthday"]));
    birthday.appendChild(birthdayNode);
    body.appendChild(birthday);

    var technologies = document.createElement('p');
    var technologiesNode = document.createTextNode(data[0]["tehnologies"]);
    technologies.appendChild(technologiesNode);
    body.appendChild(technologies);

    var id = data[0]["rm_id"];
    if (id != null) {
        var rm = document.createElement('a');
        rm.href = "detail.html?id=" + id;
        var rmNode = document.createTextNode("RM Link");
        rm.appendChild(rmNode);
        body.appendChild(rm);
    }
}

function tableCreate(employees) {
    var body = document.getElementsByTagName('body')[0];
    var table = document.createElement('table');
    table.setAttribute('border', '1');

    var tbdy = document.createElement('tbody');
    var first_name = document.createElement('th');
    first_name.appendChild(document.createTextNode("First Name"));
    tbdy.appendChild(first_name);
    var last_name = document.createElement('th');
    last_name.appendChild(document.createTextNode("Last Name"));
    tbdy.appendChild(last_name);
    var tech = document.createElement('th');
    tech.appendChild(document.createTextNode("Technology"));
    tbdy.appendChild(tech);

    var column = ["first_name", "last_name", "tehnologies"];

    for (var i = 0; i < employees.length; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < column.length; j++) {
            var td = document.createElement('td');
            td.appendChild(document.createTextNode(employees[i][column[j]]));
            tr.appendChild(td);
        }
        addButton(tr, employees[i]["id"]);
        tbdy.appendChild(tr);
    }
    table.appendChild(tbdy);
    body.appendChild(table);
}

function addButton(tr, id) {
    var btnDelete = document.createElement('input');
    btnDelete.type = "button";
    btnDelete.className = "btn";
    btnDelete.value = "Удалить";
    btnDelete.onclick = function () {
        deleteWorkerSend(id);
    };
    tr.appendChild(btnDelete);

    var btnChange = document.createElement('input');
    btnChange.type = "button";
    btnChange.className = "btn";
    btnChange.value = "Редактировать";
    btnChange.onclick = function () {
        openChangePage(id);
    };
    tr.appendChild(btnChange);

    var btnDetail = document.createElement('input');
    btnDetail.type = "button";
    btnDetail.className = "btn";
    btnDetail.value = "Подробно";
    btnDetail.onclick = function () {
        openDetailPage(id);
    };
    tr.appendChild(btnDetail);
}


function pageChangeCreate(data) {
    document.getElementById('first_name').value = data[0]["first_name"];
    document.getElementById('last_name').value = data[0]["last_name"];
    document.getElementById('birthday').placeholder = parseDate(data[0]["birthday"]);
    setTechnologies(data);
    request('allRm', getUrlParameter('id'));
}

function setRm(data) {
    var selector = document.getElementById('rm');
    for (i = 0; i < data.length; i++) {
        var selectorRm = document.createElement('option');
        selectorRm.value = data[i]["id"];
        selectorRm.appendChild(document.createTextNode(data[i]["first_name"] + " " + data[i]["last_name"]));
        selector.appendChild(selectorRm);
    }
}

function setTechnologies(data) {
    if (data[0]["tehnologies"].indexOf("Java") !== -1) {
        document.getElementById("java").checked = true;
    }
    if (data[0]["tehnologies"].indexOf("JS") !== -1) {
        document.getElementById("java_script").checked = true;
    }
    if (data[0]["tehnologies"].indexOf("Android") !== -1) {
        document.getElementById("android").checked = true;
    }
}

function openDetailPage(id) {
    window.open("detail.html?id=" + id);
}

function openChangePage(id) {
    window.open("update_detail.html?id=" + id);
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function openDetailForEmployee() {
    request(ID_WORKER, getUrlParameter('id'));
}

function openChangeForEmployee() {
    request(ID_WORKER_CHANGE, getUrlParameter('id'));
}

function send() {
    var isError = false;
    var firstName = document.getElementById("first_name").value;
    var lastName = document.getElementById("last_name").value;
    var dateForm = document.getElementById("birthday");

    var errorDate = document.getElementById("error_date");
    var errorNames = document.getElementById("error_names");
    var errorTech = document.getElementById("error_tech");

    if (checkDateForm(dateForm)) {
        var birthday = dateForm.value;
        isError = false;
        errorDate.hidden = true;
    } else {
        isError = true;
        errorDate.hidden = false;
    }
    var tech = "";
    if (document.getElementById("java").checked) {
        tech = tech + " Java";
    }
    if (document.getElementById("java_script").checked) {
        tech = tech + " JS";
    }
    if (document.getElementById("android").checked) {
        tech = tech + " Android";
    }
    if (firstName != "" && lastName != "") {
        isError = false;
        errorNames.hidden = true;
    } else {
        isError = true;
        errorNames.hidden = false;
    }
    if (tech != "") {
        isError = false;
        errorTech.hidden = true;
    } else {
        isError = true;
        errorTech.hidden = false;
    }
    var e = document.getElementById("rm");
    var rm = e.options[e.selectedIndex].value;
    if (isError == false) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', UPDATE_WORKER, true);
        xhr.setRequestHeader("id", getUrlParameter("id"));
        xhr.setRequestHeader("first_name", firstName);
        xhr.setRequestHeader("last_name", lastName);
        xhr.setRequestHeader("tehnologies", tech);
        xhr.setRequestHeader("rm_id", rm);
        xhr.setRequestHeader("birthday", birthday);

        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            if (xhr.status != 200) {
                console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
                return;
            }
            contentCreate(UPDATE_WORKER, JSON.parse(xhr.responseText));
        };
        xhr.send(null);
        window.open("index.html");
    }
}

function checkDateForm(form) {
    re = /^\d{4}-\d{1,2}-\d{1,2}$/;
    if (form.value != '' && !form.value.match(re)) {
        return false;
    } else if (form.value == '') {
        return false;
    }
    return true;
}

function deleteWorkerSend(id) {
    request(WORKER_DELETE, id);
}

function searchWorkerSend() {
    var searchText = document.getElementById("search_worker").value;
    request(SEARCH_WORKER, searchText);
}

function updateContentAfterDelete() {
    var body = document.getElementsByTagName('body')[0];
    var table = document.getElementsByTagName('table')[0];
    body.removeChild(table);
    request(ALL_WORKERS, '');
}

function updateContentAfterSearch(data) {
    var body = document.getElementsByTagName('body')[0];
    var table = document.getElementsByTagName('table')[0];
    body.removeChild(table);
    tableCreate(data);
}