var http = require('http');
var url = require('url');
var server = require('node-static');
var mysql = require('mysql');
var file = new server.Server('.');
require('/constants');

var connectInfo = {
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'employesdb',
    port: 3306
};

function accept(request, response) {
    if (request.url == ('/' + ALL_WORKERS)) {
        getWorkers(response);
    } else if (request.url == ('/' + ID_WORKER)) {
        getWorkerDetails(request, response);
    } else if (request.url == ('/' + ID_WORKER_CHANGE)) {
        getWorkerDetails(request, response);
    } else if (request.url == ('/' + ALL_RM)) {
        getRm(request, response);
    } else if (request.url == ('/' + UPDATE_WORKER)) {
        updateWorker(request, response);
    } else if (request.url == ('/' + WORKER_DELETE)) {
        deleteWorker(request, response);
    } else if (request.url == ('/' + SEARCH_WORKER)) {
        searchWorker(request, response);
    } else {
        file.serve(request, response);
    }
}

function searchWorker(request, response) {
    var sqlQuery = 'SELECT * FROM employees WHERE ' +
        'first_name LIKE \'%' + request.headers["id"] + '%\' OR ' +
        'last_name LIKE \'%' + request.headers["id"] + '%\' OR ' +
        'tehnologies LIKE \'%' + request.headers["id"] + '%\'';

    connectAndQueryToDB(response, sqlQuery);
}

function deleteWorker(request, response) {
    connectAndQueryToDB(response, 'DELETE FROM employees WHERE id =' + request.headers["id"]);
}

function updateWorker(request, response) {
    var sqlQuery = 'UPDATE employees SET first_name =\'' + request.headers["first_name"] +
        '\', last_name =\'' + request.headers["last_name"] +
        '\', tehnologies =\'' + request.headers["tehnologies"] +
        '\', rm_id =' + request.headers["rm_id"] +
        ', birthday =\'' + request.headers["birthday"] +
        '\' WHERE id =' + request.headers["id"];
    connectAndQueryToDB(response, sqlQuery);
}

function getRm(request, response) {
    connectAndQueryToDB(response, 'SELECT id, first_name, last_name FROM employees WHERE id <>' + request.headers["id"]);
}

function getWorkers(response) {
    connectAndQueryToDB(response, 'SELECT id, first_name, last_name, tehnologies, rm_id FROM employees');
}

function getWorkerDetails(request, response) {
    connectAndQueryToDB(response, 'SELECT * FROM employees WHERE id =' + request.headers["id"]);
}

function connectAndQueryToDB(response, sqlQuery) {
    var connection = mysql.createConnection(connectInfo);
    connection.connect(function (error) {
        if (error) {
            throw error;
        } else {
            console.log('Connect correct.');
        }
    });
    connection.query(sqlQuery, [1], function (error, result) {
            if (error) {
                throw error;
            } else {
                response.end(JSON.stringify(result));
            }
        }
    );
    connection.end();
}

if (!module.parent) {
    http.createServer(accept).listen(8080);
} else {
    exports.accept = accept;
}

console.log('Server running on port 8080');