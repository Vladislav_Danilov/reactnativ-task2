const ALL_WORKERS = 'allWorkers';
const ID_WORKER = 'idWorker';
const ID_WORKER_CHANGE = 'idWorkerChange';
const ALL_RM = 'allRm';
const UPDATE_WORKER = 'updateWorker';
const WORKER_DELETE = 'workerDelete';
const SEARCH_WORKER = 'searchWorker';
